package transaction.script.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.PostgresDBUtil;

public class AppConstants {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBUtil.class);
	static Properties properties;
	static {
		properties = new Properties();
		try {
			InputStream inputStream = AppConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("<< Error loading properties file:");

		}
	}

	public static final String POSTGRES_IP = properties.getProperty("postgres_host");
	public static final String TRANSACTION = "TRANSACTION";
	//Dynamo Constants
	public static final String DYNAMO_ENVIRONMENT = properties.getProperty("dynamo_environment");
	public static final String DYNAMO_FROM = properties.getProperty("dynamo_from");
	public static final String DYNAMO_HOST = properties.getProperty("dynamo_host");
	public static final String AWS_ACCESSKEY = properties.getProperty("aws_accessKey");
	public static final String AWS_SECRETKEY = properties.getProperty("aws_secretKey");
	//Table Names for postgres
	public static final String TRADER_TABLE_NAME = "kal_traderapp_transaction";
	public static final String CA_TABLE_NAME = "kal_ca_transaction";
	public static final String FARMER_TABLE_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_TABLE_NAME = "kal_ai_transaction";
	public static final String SOCIETY_TABLE_NAME = "kal_cos_transaction";
	public static final String TRANSPORTER_TABLE_NAME = "kal_transporterapp_transaction";
	public static final String WAREHOUSER_TABLE_NAME = "kal_wh_transaction";
	//Collection Names for mongo
	public static final String TRADER_COLLECTION_NAME = "kal_traderapp_transaction";
	public static final String CA_COLLECTION_NAME = "kal_ca_transaction";
	public static final String FARMER_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_COLLECTION_NAME = "kal_fertilizerapp_transaction";
	public static final String SOCIETY_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String TRANSPORTER_COLLECTION_NAME = "kal_transporter_transaction";
	public static final String WAREHOUSER_COLLECTION_NAME = "kal_warehouseapp_transaction";
	//Biz Types
	public static final String TRADER = "traderapp";
	public static final String CA = "caapp";
	public static final String FARMER = "farmerapp";
	public static final String INPUTS = "aiapp";
	public static final String SOCIETY = "cosapp";
	public static final String TRANSPORTER = "transporterapp";
	public static final String WAREHOUSE = "warehouse";
	public static final String MONGO_HOST_ONE = properties.getProperty("mongo_host");
	public static final int MONGO_HOST_PORT = Integer.parseInt(properties.getProperty("mongo_port"));
	public static final String MONGO_HOST_TWO = properties.getProperty("mongo_host_two");
	public static final String MONGO_DATABASE = properties.getProperty("common_mongo_db_name");
	public static final String MONGO_USER = properties.getProperty("mongo_user");
	public static final String MONGO_PASSWORD = properties.getProperty("mongo_password");

	//Data Correction Constants
	public static final String DELETE = "DELETE";
}