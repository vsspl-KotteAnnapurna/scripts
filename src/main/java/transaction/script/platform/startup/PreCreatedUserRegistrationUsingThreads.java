package transaction.script.platform.startup;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.models.RegistrationTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class PreCreatedUserRegistrationUsingThreads implements Runnable {
	public static final Logger LOGGER = LoggerFactory.getLogger(PreCreatedUserRegistrationUsingThreads.class);
	static PreCreatedUserRegistrationUsingThreads classObj = new PreCreatedUserRegistrationUsingThreads();
	static BlockingQueue<RegistrationTo> queue = new LinkedBlockingQueue<RegistrationTo>();
	private static Client client = ClientBuilder.newClient();
	//static WebTarget webTarget = client.target("http://192.168.1.67/rest/v1/profiles/otpusersignup");
	//static WebTarget webTarget = client.target("http://www.devkalgudi.vasudhaika.net/rest/v1/profiles/otpusersignup");
	static WebTarget webTarget = client.target("https://www.kalgudi.com/rest/v1/profiles/otpusersignup");
	static Invocation.Builder invocationBuilder = webTarget.request();
	static Thread thread = null;
	static DB db = null;
	static DBCollection collection = null;
	static Gson gson = null;
	static int numberOfUsersRegistered = 0, numberOfUserRegisterationsFailed = 0, numberOfUsersToRegisterInEachSet = 10000, numberOfThreads = 4, initialNumberOfActiveThreads = 0;
	static int numberOfUsersAlreadyRegistered = 0;
	static long startTime = 0;
	static boolean populatingQueue = false, processData = true;
	static {
		try {
			db = MongoDBUtil.getConnection();
			collection = db.getCollection("PreCreatedUsersDataColl");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/*if (args.length > 0)
			numberOfThreads = Integer.parseInt(args[0]);
		if (args.length > 1)
			numberOfUsersToRegisterInEachSet = Integer.parseInt(args[1]);*/
		initialNumberOfActiveThreads = Thread.activeCount();
		LOGGER.info(numberOfThreads + ",,,,,," + numberOfUsersToRegisterInEachSet + ",,,,,," + initialNumberOfActiveThreads);
		try {
			while (true) {
				populateQueue();
				if (startTime == 0)
					startTime = System.currentTimeMillis();
				if (queue.size() == 0)
					break;
				spawnThreads();
				while (true) {
					if (queue.size() == 0 && (Thread.activeCount() - initialNumberOfActiveThreads) == 0) {
						TimeUnit.SECONDS.sleep(120);
						LOGGER.info("Finished waiting for queues and threads to become zero");
						break;
					}
				}
				if ((numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) >= (numberOfUsersToRegisterInEachSet * 10) && queue.size() == 0)
					break;
			}
			LOGGER.info("Abhishek signing off....");
		} catch (Exception e) {
			LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void populateQueue() {
		populatingQueue = true;
		LOGGER.info("\n\n\nStarted populating queues");
		List<BasicDBObject> list = new ArrayList<BasicDBObject>();
		BasicDBObject searchQuery = new BasicDBObject();
		list.add(new BasicDBObject("mobileNumber", new BasicDBObject("$regex", "^((?!910000).)*$")));
		list.add(new BasicDBObject("isPreCreatedRegComplete", false));
		searchQuery.put("$and", list);
		long mongoGetStartTime = System.currentTimeMillis();
		DBCursor cursor = collection.find(searchQuery).sort(new BasicDBObject("_id", 1)).limit(numberOfUsersToRegisterInEachSet);
		LOGGER.info("Mongo records fetching time is::: " + (System.currentTimeMillis() - mongoGetStartTime));
		//DBCursor cursor = collection.find(new BasicDBObject("isPreCreatedRegComplete", false)).sort(new BasicDBObject("_id", 1)).limit(numberOfUsersToRegisterInEachSet);
		long queueFillingStartTime = System.currentTimeMillis();
		while (cursor.hasNext()) {
			String data = cursor.next().toString();
			RegistrationTo objRegistrationTo = gson.fromJson(data, RegistrationTo.class);
			queue.offer(objRegistrationTo);
		}
		LOGGER.info("Queue is filled with " + numberOfUsersToRegisterInEachSet + " records in ::" + (System.currentTimeMillis() - queueFillingStartTime));
		populatingQueue = false;
		LOGGER.info("Current queue size is :: " + queue.size());
	}

	@Override
	public void run() {
		LOGGER.info("Current thread count is::" + Thread.activeCount());
		while (!Thread.currentThread().isInterrupted()) {
			while (populatingQueue) {
				try {
					TimeUnit.SECONDS.sleep(20);
				} catch (InterruptedException e) {
					LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
					e.printStackTrace();
				}
			}
			if (numberOfUsersRegistered > 0 && numberOfUsersRegistered % 100 == 0) {
				LOGGER.info("Superman in work...");
				LOGGER.info(numberOfUsersRegistered + " users registered until now. Time elapsed is :: " + (System.currentTimeMillis() - startTime) + "ms\n\n\n");
			}
			if (queue.size() == 0) {
				while (Thread.currentThread().isInterrupted())
					Thread.currentThread().interrupt();
				if (Thread.activeCount() - initialNumberOfActiveThreads == 1) {
					LOGGER.info("Superman saves the day.All data is processed.");
					LOGGER.info("Total number of users registered successfully :: " + numberOfUsersRegistered);
					LOGGER.info("Total number of users who are already registered with us :: " + numberOfUsersAlreadyRegistered);
					LOGGER.info("Total number of user registrations failed :: " + numberOfUserRegisterationsFailed);
					LOGGER.info("Total time taken to process " + (numberOfUsersRegistered + numberOfUserRegisterationsFailed + numberOfUsersAlreadyRegistered) + " is :: "
							+ (System.currentTimeMillis() - startTime) + "ms");
				}
				break;
			}
			try {
				RegistrationTo objRegistrationTo = queue.poll();
				if (objRegistrationTo != null)
					hitPreCreatedUserEndpoint(objRegistrationTo);
			} catch (Exception e) {
				LOGGER.error("\n\n\n\n\n\n\n******************************************\n" + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private static void hitPreCreatedUserEndpoint(RegistrationTo registrationTo) {
		long serviceCallStartTime = System.currentTimeMillis();
		Response clientResponse = invocationBuilder.post(Entity.entity(gson.toJson(registrationTo), MediaType.APPLICATION_JSON));
		LOGGER.info(Thread.currentThread().getName() + "::: Service Response Time is:::" + (System.currentTimeMillis() - serviceCallStartTime) + " ms");
		JSONObject jsonObject = new JSONObject(clientResponse.readEntity(String.class));
		long mongoUpdateStartTime = System.currentTimeMillis();
		if (jsonObject.get("code").toString().equals("200")) {
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Success::" + jsonObject.get("info").toString());
			collection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class), false, true);
			numberOfUsersRegistered++;
		} else {
			String responseData = jsonObject.get("error").toString();
			registrationTo.setIsPreCreatedRegComplete(true);
			registrationTo.setPreCreatedRegInfo("Error::" + jsonObject.get("error").toString());
			collection.update(new BasicDBObject("mobileNumber", registrationTo.getMobileNumber()), gson.fromJson(gson.toJson(registrationTo), BasicDBObject.class), false, true);
			if (responseData.toLowerCase().contains("already registered"))
				numberOfUsersAlreadyRegistered++;
			else {
				LOGGER.info("bad things are happening::: " + responseData);
				numberOfUserRegisterationsFailed++;
			}
		}
		LOGGER.info(Thread.currentThread().getName() + "::: Mongo update Time is :::" + (System.currentTimeMillis() - mongoUpdateStartTime + " ms"));
	}

	private static void spawnThreads() {
		for (int i = 0; i < numberOfThreads; i++) {
			thread = new Thread(classObj, "TID : " + (i + 1));
			thread.start();
		}
	}
}