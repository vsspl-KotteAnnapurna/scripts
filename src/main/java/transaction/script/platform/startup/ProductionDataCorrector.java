package transaction.script.platform.startup;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.models.MasterCollectionTo;
import transaction.script.models.UserProfileDataTo;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;

public class ProductionDataCorrector {
	public static final Logger LOGGER = LoggerFactory.getLogger(PreCreatedUserRegistrationUsingThreads.class);
	static DB db = null;
	static DBCollection productionCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static boolean productionMobileNumberCheck = false;
	static int numberOfUsersEligibleForDeletion = 0;
	static {
		try {
			db = MongoDBUtil.getConnection();
			productionCollection = db.getCollection("productionCollection");
			masterCollection = db.getCollection("masterCollection");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise data");
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		int limitForProductionGetQuery = 10;
		int offsetForProductionGetQuery = 0;
		List<BasicDBObject> productionGetQuery = new ArrayList<BasicDBObject>();
		productionGetQuery.add(new BasicDBObject("mobileNo", 1));
		productionGetQuery.add(new BasicDBObject("_id", -1));
		System.out.println(gson.toJson(new BasicDBObject("$and", productionGetQuery)));
		DBCursor productionData = productionCollection.find(new BasicDBObject()).sort(new BasicDBObject("mobileNo", 1).append("_id", -1)).limit(limitForProductionGetQuery)
				.skip(offsetForProductionGetQuery);
		while (productionData.hasNext()) {
			UserProfileDataTo userProfileData = gson.fromJson(productionData.next().toString(), UserProfileDataTo.class);
			System.out.println(gson.toJson(userProfileData));
			/*if (productionMobileNumberCheck) {
				DBCursor masterCollectionDbCursor = masterCollection.find(new BasicDBObject("mobileNumber", userProfileData.getMobileNo()));
				while (masterCollectionDbCursor.hasNext()) {
					masterCollectionDbCursor.next();
					//Code has to be written
				}
			}*/
			/*List<BasicDBObject> list = new ArrayList<BasicDBObject>();
			BasicDBObject searchQuery = new BasicDBObject();
			list.add(new BasicDBObject("name", "/^" + userProfileData.getFirstName() + "$/i"));
			list.add(new BasicDBObject("businessName", "/^" + userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessName() + "$/i"));
			list.add(new BasicDBObject("businessTypeId", "/^" + userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getBusinessTypeId() + "$/i"));
			list.add(new BasicDBObject("locationName", "/^" + userProfileData.getLstOfUserBusinessDetailsInfo().get(0).getLocationTo().getPlaceName() + "$/i"));
			searchQuery.put("$and", list);
			DBCursor masterCollectionDbCursor = masterCollection.find(searchQuery);
			while (masterCollectionDbCursor.hasNext()) {
				MasterCollectionTo masterCollectionTo = gson.fromJson(masterCollectionDbCursor.next().toString(), MasterCollectionTo.class);
				masterCollectionTo.setStatus(AppConstants.DELETE);
				masterCollection.update(new BasicDBObject("_id", masterCollectionTo.get_id()), gson.fromJson(gson.toJson(masterCollectionTo), BasicDBObject.class));
			}*/
		}
	}
}