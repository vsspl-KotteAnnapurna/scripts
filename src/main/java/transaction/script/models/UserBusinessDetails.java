package transaction.script.models;

public class UserBusinessDetails {
	private String businessName;
	private String businessTypeId;
	private PlaceTo locationTo;

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	public PlaceTo getLocationTo() {
		return locationTo;
	}

	public void setLocationTo(PlaceTo locationTo) {
		this.locationTo = locationTo;
	}

}
